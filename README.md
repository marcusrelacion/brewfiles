# Brewfiles

My personal MacOS Brewfiles for quickly performing fresh-installs or configuring new personal and work machines

## Instructions

Install [Homebrew](https://brew.sh/):

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

Create Brewfile:

`brew bundle dump`

Run Brewfile:

`brew bundle`

Below is a list of applications that need to be installed manually:

**Open Source:**

- Nothing currently

**Free Personal:**

- [Display Link Adapter](https://www.synaptics.com/products/displaylink-graphics/downloads/macos)
- [Streamdeck](https://edge.elgato.com/egc/macos/sd/Stream_Deck_5.2.1.15025.pkg?_ga=2.240002433.388066824.1650310082-2065781075.1650310082)

**Paid Personal**

- [Darkroom](https://darkroom.co/)
- [iA Writer](https://ia.net/writer)
- [iA Presenter](#)
- [Keystroke Pro](https://apps.apple.com/us/app/keystroke-pro/id1572206224?mt=12)
- [Photo Mechanic](https://home.camerabits.com/)
- [Raw Right Away](https://apps.apple.com/us/app/raw-right-away/id963507809?mt=12)
- [Theine](https://apps.apple.com/us/app/theine/id955848755?mt=12)
